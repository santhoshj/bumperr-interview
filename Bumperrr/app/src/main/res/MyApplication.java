import android.app.Application;

/**
 * Created by SJ045822 on 11-06-2016.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
