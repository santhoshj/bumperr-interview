package com.sjdroid.bumperrr;

import java.util.List;

/**
 * Created by SJ045822 on 12-06-2016.
 */

public class MarketplaceModel {
    String cursor;
    String errorMessage;
    boolean sucess;
    boolean more;

    @Override
    public String toString() {
        return "MarketplaceModel{" +
                "cursor='" + cursor + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", sucess='" + sucess + '\'' +
                ", more='" + more + '\'' +
                ", Product=" + Product +
                '}';
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean getSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public boolean getMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public List<product> getProduct() {
        return Product;
    }

    public void setProduct(List<product> product) {
        Product = product;
    }

    List<product> Product;

    public class product {

        String name;
        String Id;
        String created;
        String image;
        String price;


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "product{" +
                    "name='" + name + '\'' +
                    ", Id='" + Id + '\'' +
                    ", created='" + created + '\'' +
                    ", image='" + image + '\'' +
                    ", price='" + price + '\'' +
                    '}';
        }
    }

}
