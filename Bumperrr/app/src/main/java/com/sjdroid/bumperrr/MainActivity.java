package com.sjdroid.bumperrr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.io.IOException;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "IM19jeQt3ccHchw14KUnv8nfB";
    private static final String TWITTER_SECRET = "TN2eCufbV8onm4o161cQFyYAGgvvUYsKUo6CDs0nbmq4SRjJGz";
    private static final String TWITTER_CONSUMER_KEY = "BJnRhaYM3uuBQyjKPcwuuDvKS";
    private static final String TWITTER_CONSUMER_SECRET = "2nGnIrJ232hoXSToq2ypkDPWklsU0HoIRvskjGNxjglA0BBsrr";

    private Button loginButton;
    private TwitterAuthClient client;
    private ProgressBar progressBar;
    private Animation animSlide;
    public String secret;
    public String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("test", Context.MODE_PRIVATE);
        boolean loggedIn = sharedPref.getBoolean("login", false);

        Log.d("logged status", loggedIn + "");
        if (loggedIn) {
            Intent it = new Intent(MainActivity.this, Main2Activity.class);
            startActivity(it);
            finish();
        }

        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        client = new TwitterAuthClient();


        setContentView(R.layout.activity_main);

        ImageView img_animation = (ImageView) findViewById(R.id.imageView2);
        // Load the animation like this
        animSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide);
        animSlide.setRepeatCount(5);
        animSlide.setRepeatCount(2);
        img_animation.startAnimation(animSlide);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        loginButton = (Button) findViewById(R.id.button3);
        loginButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                client.authorize(MainActivity.this, new Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> result) {

                        TwitterSession mSession = result.data;
                        token = mSession.getAuthToken().token;
                        secret = mSession.getAuthToken().secret;
                        Toast.makeText(getApplicationContext(), token + " Logged in", Toast.LENGTH_LONG).show();
                        new NetworkCall().execute();
                    }

                    @Override
                    public void failure(TwitterException exception) {

                    }
                });
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        client.onActivityResult(requestCode, resultCode, data);
    }


    private class NetworkCall extends android.os.AsyncTask<String, String, String> {
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(MainActivity.this);
            progress.setIndeterminate(true);
            progress.setMessage("Logging In");
            progress.show();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progress.hide();


            Intent it = new Intent(getApplicationContext(), Main2Activity.class);
            startActivity(it);

        }

        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();

            Log.d("secret", secret);
            Log.d("token", token);
            secret = "JmUWuyUOReD662rfJvWQUBDADuffIVHW43VJ3dZukd2LO";
            token="570564753-2BIADKjAGT1NhnhNSmHMfGD28WQs6Hgnqq8sJV5p";


            MediaType mediaType = MediaType.parse("multipart/form-data; boundary=---011000010111000001101001");
            /*RequestBody body = RequestBody.create(mediaType, "-----011000010111000001101001\r\nContent-Disposition:" +
                    " form-data; name=\"twitterSecret\"\r\n\r\n" + secret +
                    "\r\n-----011000010111000001101001\r\nContent-Disposition:" +
                    " form-data; name=\"accessToken\"\r\n\r\n" + token + "\r\n-----011000010111000001101001--");
         */
            RequestBody body = new FormBody.Builder()
                    .add("twitterSecret", secret)
                    .add("accessToken", token)
                    .build();
            Request request = new Request.Builder()
                    .url("https://bumperrrhiring.appspot.com/login")
                    .post(body)
                    .addHeader("content-type", "multipart/form-data; boundary=---011000010111000001101001")
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                okhttp3.Response response = client.newCall(request).execute();
                Log.d("response code", String.valueOf(response.code()));
                Gson gson = new Gson();
                LoginDetails obj2 = gson.fromJson(response.body().string(), LoginDetails.class);
                Log.d("response login details", obj2.toString());

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("test", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("login", true);
                editor.putString("id", obj2.getUser().getId());
                editor.putString(getString(R.string.imageurl), obj2.getUser().getImage());
                editor.putString("username", obj2.getUser().getTwitterName());
                editor.commit();


            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }
    }

}
