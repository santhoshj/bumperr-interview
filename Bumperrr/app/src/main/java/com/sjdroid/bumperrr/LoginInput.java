package com.sjdroid.bumperrr;

/**
 * Created by SJ045822 on 09-06-2016.
 */

public class LoginInput {

    private String accessToken;
    private String twitterSecret;

    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public String toString() {
        return "LoginInput{" +
                "accessToken='" + accessToken + '\'' +
                ", twitterSecret='" + twitterSecret + '\'' +
                '}';
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTwitterSecret() {
        return twitterSecret;
    }

    public void setTwitterSecret(String twitterSecret) {
        this.twitterSecret = twitterSecret;
    }
}
