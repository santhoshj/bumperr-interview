package com.sjdroid.bumperrr;

/**
 * Created by SJ045822 on 08-06-2016.
 */

public class LoginDetails {

    String errorMessage;
    String success;
    Muser user;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSucess() {
        return success;
    }

    public void setSucess(String success) {
        this.success = success;
    }

    public Muser getUser() {
        return user;
    }

    public void setUser(Muser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "LoginDetails{" +
                "errorMessage='" + errorMessage + '\'' +
                ", sucess='" + success + '\'' +
                ", user=" + user +
                '}';
    }

    class Muser{
        String twitterId;
        String name;
        String twitterName;
        String image;
        String created;
        String Id;

        public String getTwitterId() {
            return twitterId;
        }

        public void setTwitterId(String twitterId) {
            this.twitterId = twitterId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTwitterName() {
            return twitterName;
        }

        public void setTwitterName(String twitterName) {
            this.twitterName = twitterName;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        @Override
        public String toString() {
            return "user{" +
                    "twitterId='" + twitterId + '\'' +
                    ", name='" + name + '\'' +
                    ", twitterName='" + twitterName + '\'' +
                    ", image='" + image + '\'' +
                    ", created='" + created + '\'' +
                    ", Id='" + Id + '\'' +
                    '}';
        }
    }



}
