package com.sjdroid.bumperrr;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String mimageurl;
    private String ApiId;
    private String cursor = "";
    private RecyclerView mRecyclerView;
    private boolean loadMore;
    private List<MarketplaceModel.product> products;

    private boolean Loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        products = new ArrayList<>();
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("test", Context.MODE_PRIVATE);

        ApiId = sharedPref.getString("id", "");

        Log.d("Appid", ApiId);

        new NetworkCall().execute();

        mimageurl = sharedPref.getString(String.valueOf(R.string.imageurl), "");
        updateview();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    if ((mLayoutManager.getChildCount() + mLayoutManager.findFirstVisibleItemPosition()) >= mLayoutManager.getItemCount()) {
                        Log.d("TAG", "End of list");
                        LoadMore();
                    }
                }
            }

        });


    }

    private void LoadMore() {
        if(!Loading) {
            Loading=true;
            new NetworkCall().execute();
        }
    }


    private void updateview() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void Loadmore() {
        if (loadMore)
            new NetworkCall().execute();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class NetworkCall extends android.os.AsyncTask<String, String, String> {

        private MyAdapter mAdapter;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Gson gson = new Gson();
            MarketplaceModel marketplaceModel = gson.fromJson(s, MarketplaceModel.class);
            try {
                if (marketplaceModel.getSucess()) {
                    cursor = marketplaceModel.getCursor();
                    loadMore = marketplaceModel.getMore();
                    Log.d("marketplace api", marketplaceModel.toString());

                    for (MarketplaceModel.product product :
                            marketplaceModel.getProduct()) {
                        products.add(product);
                    }

                    Loading=false;
                         mAdapter = new MyAdapter(products, Main2Activity.this);
                        mRecyclerView.setAdapter(mAdapter);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            if (cursor == null)
                return null;

            Response response;
            OkHttpClient client = new OkHttpClient();


            MediaType mediaType = MediaType.parse("multipart/form-data; boundary=---011000010111000001101001");
            //   RequestBody body = RequestBody.create(mediaType, "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"id\"\r\n\r\n5656058538229760\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"cursor\"\r\n\r\nCi8SKWoQc35idW1wZXJycmhpcmluZ3IVCxIIUHJvZHVjdHMYgICAgLzVkwoMGAAgAA==\r\n-----011000010111000001101001--");


            RequestBody body = new FormBody.Builder()
                    .add("id", ApiId)
                    .add("cursor", cursor)
                    .build();

            Request request = new Request.Builder()
                    .url("https://bumperrrhiring.appspot.com/marketplace")
                    .post(body)
                    .addHeader("content-type", "multipart/form-data; boundary=---011000010111000001101001")
                    .addHeader("cache-control", "no-cache")
                    .addHeader("postman-token", "6f2af1f2-b0c8-b359-92ce-d74f9e0735af")
                    .build();

            try {
                response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
}
