package com.sjdroid.bumperrr;

import com.twitter.sdk.android.core.models.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by SJ045822 on 08-06-2016.
 */

public interface ApiService {


    @POST("/login")
    Call<LoginDetails> Login_User(@Body LoginInput mLoginInput);


}
