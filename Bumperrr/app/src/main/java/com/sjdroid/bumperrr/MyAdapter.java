package com.sjdroid.bumperrr;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.Image;

import java.util.List;

/**
 * Created by SJ045822 on 12-06-2016.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private final Context context;
    private List<MarketplaceModel.product> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView productImage;
        public TextView productName;
        public TextView productPrice;
        public ViewHolder(View v) {
            super(v);
            productImage= (ImageView) v.findViewById(R.id.productimage);
            productName= (TextView) v.findViewById(R.id.productname);
            productPrice= (TextView) v.findViewById(R.id.productprice);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<MarketplaceModel.product> myDataset, Context context) {
        mDataset = myDataset;
        this.context=context;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.productName.setText(mDataset.get(position).getName());
        holder.productPrice.setText(" ₹"+ mDataset.get(position).getPrice());
        holder.productImage.setMinimumHeight(holder.productImage.getWidth());
        Picasso.with(context).load(mDataset.get(position).getImage()).into(holder.productImage);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
